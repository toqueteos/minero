// Defines important constants such as: latest NMS supported version.
package minero

const (
	ProtoNum = 60
	Proto    = "60"
	Server   = "1.5"
)
