// Package constants defines enum-like types.
//
// Examples: Materials (Block Ids), Potion Types, Inventory Types, Player
// Actions, etc...
package constants
