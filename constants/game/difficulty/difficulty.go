// Package difficulty
package difficulty

const (
	Peaceful = iota
	Easy
	Normal
	Hard
)
